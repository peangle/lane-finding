#importing some useful packages
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import cv2
import math
import sys
import os
from moviepy.editor import VideoFileClip
import scipy.misc

def grayscale(img):
    """Applies the Grayscale transform
    This will return an image with only one color channel
    but NOTE: to see the returned image as grayscale
    you should call plt.imshow(gray, cmap='gray')"""
    return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


def canny(img, low_threshold, high_threshold):
    """Applies the Canny transform"""
    return cv2.Canny(img, low_threshold, high_threshold)


def gaussian_blur(img, kernel_size):
    """Applies a Gaussian Noise kernel"""
    return cv2.GaussianBlur(img, (kernel_size, kernel_size), 0)


def region_of_interest(img, vertices):
    """
    Applies an image mask.

    Only keeps the region of the image defined by the polygon
    formed from `vertices`. The rest of the image is set to black.
    """
    # defining a blank mask to start with
    mask = np.zeros_like(img)

    # defining a 3 channel or 1 channel color to fill the mask with depending on the input image
    if len(img.shape) > 2:
        channel_count = img.shape[2]  # i.e. 3 or 4 depending on your image
        ignore_mask_color = (255,) * channel_count
    else:
        ignore_mask_color = 255

    # filling pixels inside the polygon defined by "vertices" with the fill color
    cv2.fillPoly(mask, vertices, ignore_mask_color)

    # returning the image only where mask pixels are nonzero
    masked_image = cv2.bitwise_and(img, mask)
    return masked_image

def find_lane_lines(image, lines):
    if lines is None:
        return lines
    slopes_map = {}
    for line in lines:
        for x1, y1, x2, y2 in line:
            if x1 == x2:
                continue
            slope = (y2 - y1) / (x2 - x1)
            slope_lines = slopes_map.get(slope)
            if not slope_lines:
                slope_lines = []
                slopes_map[slope] = slope_lines
            slope_lines.append(line)
    slopes = list(slopes_map)
    lines_above_threshold = []
    lines_below_threshold = []
    mean = np.mean(slopes)
    deviation_offset = 0.5 * np.std(slopes)
    while True:
        upper_threshold = mean + deviation_offset
        lower_threshold = mean - deviation_offset
        for slope in slopes_map:
            slope_lines = slopes_map.get(slope)
            if slope >= upper_threshold:
                lines_above_threshold.extend(slope_lines)
            elif slope <= lower_threshold:
                lines_below_threshold.extend(slope_lines)
        if lines_above_threshold and lines_below_threshold:
            break
        else:
            deviation_offset *= 0.75

    line_above_threshold = get_fitting_line_based_on_distance_from_origin(image, lines_above_threshold)
    line_below_threshold = get_fitting_line_based_on_distance_from_origin(image, lines_below_threshold)
    lane_lines = np.array([line_above_threshold, line_below_threshold])

    return lane_lines

def get_fitting_line_based_on_distance_from_origin(image, lines):
    global frame
    x = []
    y = []
    distance_map = {}
    for line in lines:
        for x1, y1, x2, y2 in line:
            y2_y1 = y2 - y1
            x2_x1 = x2 - x1
            distance_from_origin = (x2*y1-y2*x1)/(math.sqrt(y2_y1*y2_y1 + x2_x1*x2_x1))
            distance_map[distance_from_origin] = (x1, y1, x2, y2)

    distances = list(distance_map)
    offset = np.std(distances)
    mean = np.mean(distances)

    while True:
        upper_threshold = mean + offset
        lower_threshold = mean - offset
        for distance in distances:
            if distance < lower_threshold or distance > upper_threshold:
                continue
            x1, y1, x2, y2 = distance_map[distance]
            x.append(x1)
            x.append(x2)
            y.append(y1)
            y.append(y2)

        if x and y:
            break
        else:
            offset += offset/2

    m, b = np.polyfit(x, y, 1)

    start_y = image.shape[0]
    start_x = math.floor((start_y - b)/m)
    end_y = math.floor(image.shape[0] * 0.6)
    end_x = math.floor((end_y - b)/m)
    return np.array([[start_x, start_y, end_x, end_y]])

def get_fitting_line_based_on_slope(image, lines):
    global frame
    x = []
    y = []
    slopes_map = {}
    for line in lines:
        for x1, y1, x2, y2 in line:
            if (x2-x1) is not 0:
                slopes_map[((y2-y1)/(x2-x1))] = line
    if frame == 278 or frame == 619:
        print("Slopes - {0}".format(sorted(slopes_map)))
    slopes = list(slopes_map)
    offset = np.std(slopes)
    mean = np.mean(slopes)

    slopes = []
    while True:
        upper_threshold = mean + offset
        lower_threshold = mean - offset
        for slope in slopes_map:
            if slope < lower_threshold or slope > upper_threshold:
                continue
            for x1, y1, x2, y2 in slopes_map[slope]:
                x.append(x1)
                x.append(x2)
                y.append(y1)
                y.append(y2)
            slopes.append(slope)
        if x and y:
            break
        else:
            offset += offset/2

    if frame == 619:
        print("x - {0}".format(x))
        print("y - {0}".format(y))
        print("frame - {0}, m,b - ({1},{2})".format(frame, m, b))

    m, b = np.polyfit(x, y, 1)
    if not ((-1 < m < -0.4) or (0.4 < m < 1)):
        print("frame - {0}, m,b - ({1},{2})".format(frame, m, b))

    start_y = image.shape[0]
    start_x = math.floor((start_y - b)/m)
    end_y = math.floor(image.shape[0] * 0.6)
    end_x = math.floor((end_y - b)/m)
    return np.array([[start_x, start_y, end_x, end_y]])


def draw_lines(img, lines, color=[255, 0, 0], thickness=10):
    """
    NOTE: this is the function you might want to use as a starting point once you want to
    average/extrapolate the line segments you detect to map out the full
    extent of the lane (going from the result shown in raw-lines-example.mp4
    to that shown in P1_example.mp4).

    Think about things like separating line segments by their
    slope ((y2-y1)/(x2-x1)) to decide which segments are part of the left
    line vs. the right line.  Then, you can average the position of each of
    the lines and extrapolate to the top and bottom of the lane.

    This function draws `lines` with `color` and `thickness`.
    Lines are drawn on the image inplace (mutates the image).
    If you want to make the lines semi-transparent, think about combining
    this function with the weighted_img() function below
    """
    if lines is None:
        return
    for line in lines:
        for x1, y1, x2, y2 in line:
            cv2.line(img, (x1, y1), (x2, y2), color, thickness)


def hough_lines(img, rho, theta, threshold, min_line_len, max_line_gap):
    """
    `img` should be the output of a Canny transform.

    Returns an image with hough lines drawn.
    """
    lines = cv2.HoughLinesP(img, rho, theta, threshold, np.array([]), minLineLength=min_line_len,
                            maxLineGap=max_line_gap)
    lines = find_lane_lines(img, lines)
    line_img = np.zeros((*img.shape, 3), dtype=np.uint8)
    draw_lines(line_img, lines)
    return line_img


# Python 3 has support for cool math symbols.

def weighted_img(img, initial_img, α=0.8, β=1., λ=0.):
    """
    `img` is the output of the hough_lines(), An image with lines drawn on it.
    Should be a blank image (all black) with lines drawn on it.

    `initial_img` should be the image before any processing.

    The result image is computed as follows:

    initial_img * α + img * β + λ
    NOTE: initial_img and img must be the same shape!
    """
    return cv2.addWeighted(initial_img, α, img, β, λ)


def paint_lanes(img):
    gray_img = grayscale(img)
    blurred_img = gaussian_blur(gray_img, 3)
    canny_img = canny(blurred_img, 50, 150)

    ysize = img.shape[0]
    xsize = img.shape[1]
    vertices = np.array(
        [[(xsize * .1, ysize), (xsize * .45, ysize * .60), (xsize * .55, ysize * .60), (xsize * .9, ysize)]],
        dtype=np.int32)
    masked_canny_img = region_of_interest(canny_img, vertices)

    line_img = hough_lines(masked_canny_img, 2, np.pi / 180, 10, 25, 20)

    return weighted_img(line_img, img)

frame = 0
def process_image(image):
    global frame
    frame += 1
    # NOTE: The output you return should be a color image (3 channel) for processing video below
    # TODO: put your pipeline here,
    # you should return the final output (image with lines are drawn on lanes)
    if frame == 281:
        scipy.misc.imsave('outfile_{0}_before.jpg'.format(frame), image)

    image = paint_lanes(image)

    if frame == 281:
        scipy.misc.imsave('outfile_{0}_after.jpg'.format(frame), image)

    return image

def test_images():
    i = 1
    file_list = os.listdir("test_images/")
    plt.interactive(False)
    for file in file_list:
        print(file)
        #image = mpimg.imread('test_images/%s' % file)
        image = mpimg.imread('test_images/outfile_281_before.jpg')
        #plt.subplot(4, 4, i)
        i += 1
        plt.imshow(paint_lanes(image))
        plt.show()
        break

def process_solid_white_right():
    white_output = 'white.mp4'
    clip1 = VideoFileClip("solidWhiteRight.mp4")
    white_clip = clip1.fl_image(process_image) #NOTE: this function expects color images!!
    white_clip.write_videofile(white_output, audio=False)

def process_solid_yello_left():
    yellow_output = 'yellow.mp4'
    clip2 = VideoFileClip('solidYellowLeft.mp4')
    yellow_clip = clip2.fl_image(process_image)
    yellow_clip.write_videofile(yellow_output, audio=False)

def process_challenge():
    challenge_output = 'extra.mp4'
    clip2 = VideoFileClip('challenge.mp4')
    challenge_clip = clip2.fl_image(process_image)
    challenge_clip.write_videofile(challenge_output, audio=False)

test_images()
#process_solid_white_right()
#process_solid_yello_left()
#process_challenge()
